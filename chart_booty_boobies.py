import plotly.graph_objects as go
import os
import json

artists_list = os.listdir('data/')
ass_related = 'ass butts butt booty booties arse bum'.split()
boobs_related = 'boob boobs nipple nipples breast tits titties tit melons chest breasts hooters bust bosom'.split()

def countByRelated(relatedlist, artists):
    returndic = {}
    for a in artists:
        result = 0
        with open('data/' + a, 'r') as file:
            dic = json.load(file)
            for w in relatedlist:
                try:
                    result += dic[w]
                except:
                    pass
        returndic[a[:-4]] = result
    return returndic

def getResultsbyQuery(query):
    results = []
    for i in artists:
        with open('data/' + i + '.txt', 'r') as file:
            dic = json.load(file)
            try:
                results.append(dic[query])
            except:
                results.append(0)
    return results

def fit01(data):
    new = []
    for i in data:
        new.append((float(i) - float(min(data))) / (float(max(data)) - float(min(data))))
    return new

def placeLines():
    for i in range(0, len(artists_list)):
        x_val = list(ass_results.values())[i]
        y_val = list(boobs_results.values())[i]
        fig.add_trace(go.Scatter(
            x=[0, x_val, x_val], 
            y=[y_val, y_val, 0],
            mode='lines',
            line_color='cadetblue',
            line_width=1,
            line_dash='dot'
            )
        )


ass_results = countByRelated(ass_related, artists_list)
boobs_results = countByRelated(boobs_related, artists_list)

fig = go.Figure()

placeLines()

fig.add_trace(go.Scatter(
            x=list(ass_results.values()),
            y=list(boobs_results.values()),
            text=list(ass_results.keys()),
            textposition = 'top center',
            line_dash='solid',
            textfont_family="Verdana",
            textfont_size=20,
            mode='markers+text'
        )
    )

fig.update_traces(
    marker_size=20,
    marker_line_width=2,
    marker_symbol=200,
    marker_color=fit01(ass_results.values()),
    marker_colorscale='Darkmint'
)

fig.update_layout(
    title='Bum and breast descriptive words',
    font_size=30,
    xaxis_title = 'Bum',
    yaxis_title = 'Breast',
    showlegend=False
)

fig.update_xaxes(rangemode="tozero")
fig.update_yaxes(rangemode="tozero")

fig.show()


fig.write_image("output/image_bootyboobies.png", width=4096, height=2048)