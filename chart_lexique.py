import plotly.graph_objects as go
import json
import os

artists_list = os.listdir('data/')
artists_names = []

def getLexicalCount(artists):
    liste = []
    for i in artists:
        with open('data/' + i , 'r') as file:
            dic = json.load(file)
            liste.append(len(dic))
        artists_names.append(i[:-4] + ' ')
    return liste

def drawFigure():
    fig.add_trace(go.Bar(
    x=getLexicalCount(artists_list),
    y=artists_names,
    name='Number of differents words',
    orientation='h',
    marker_color=alternateColors('#449DD1', '#78C0E0', len(artists_list))
    ))

def alternateColors(color1, color2, number):
    liste = []
    for i in range(0, number):
        if i%2 == 0:
            liste.append(color1)
        else:
            liste.append(color2)
    return liste

fig = go.Figure()
drawFigure()

fig.update_layout(
    title='Number of differents words used',
    xaxis_title = 'Number of words'
)

# Here we modify the tickangle of the xaxis, resulting in rotated labels.
fig.show()
fig.write_image("output/image_lexique.png", width=1080, height=1500)