import plotly.graph_objects as go
import json

artists = ['Jul', 'PNL', 'Ninho', 'Gims', 'Damso', 'Nekfeu', 'Booba', 'Niska', 'Orelsan', 'Vald']

def getResultsbyQuery(query):
    results = []
    for i in artists:
        with open('dataFR/' + i + '.txt', 'r') as file:
            dic = json.load(file)
            tres = 0
            try:
                tres = dic[query]
            except:
                tres = 0
            try:
                tres += dic[query+'s']
            except:
                pass
        results.append(tres)
    return results

def drawFigure(query, color):
    fig.add_trace(go.Bar(
    x=artists,
    y=getResultsbyQuery(query),
    name= query[:1].upper() + query[1:],
    marker_color=color
    ))



fig = go.Figure()

drawFigure('pute', '#ffbc42')
drawFigure('cul', '#d81159')
drawFigure('putain', '#8f2d56')
drawFigure('niquer', '#218380')
drawFigure('merde', '#73d2de')
drawFigure('chatte', '#4361ee')

# Here we modify the tickangle of the xaxis, resulting in rotated labels.
fig.update_layout(barmode='group', xaxis_tickangle=0)
fig.show()
fig.write_image("image_FR.png", width=1920, height=1080)