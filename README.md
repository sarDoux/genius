# Genius scraping

I fetched the 50 most popular songs of the most famous anglophone rappers.
I then proceeded to make various charts with it.

## Tools

`Genius API` for the lyrics fetching, `plot.ly` for the charts.

## Results

![difwords](https://i.imgur.com/Lgjf1yS.png)
![totalwords](https://i.imgur.com/juKLTvn.png)
![lexicalcomp](https://i.imgur.com/Pl7SjkS.png)
![bnbcomparative](https://i.imgur.com/9tggnpn.png)
![gencomparative](https://i.imgur.com/7wTiOe5.png)
![ladcomparative](https://i.imgur.com/n5CGPI0.png)
![parcomparative](https://i.imgur.com/GIE8RWj.png)

## License
[MIT](https://choosealicense.com/licenses/mit/)