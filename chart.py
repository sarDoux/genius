import plotly.graph_objects as go
import json

artists = ['Eminem', 'Jay-Z', '50 cent', 'Lil Wayne', 'Travis Scott', 'Snoop Dogg', 'Future', 'Drake', 'Kanye West', 'Nas', 'Young Thug', 'Kendrick Lamar', 'Will Smith']


def getResultsbyQuery(query):
    results = []
    for i in artists:
        with open('data/' + i + '.txt', 'r') as file:
            dic = json.load(file)
            try:
                results.append(dic[query])
            except:
                results.append(0)
    return results

def drawFigure(query, color):
    fig.add_trace(go.Bar(
    x=artists,
    y=getResultsbyQuery(query),
    name= query[:1].upper() + query[1:],
    marker_color=color
    ))



fig = go.Figure()

drawFigure('bitch', '#ffbc42')
drawFigure('nigga', '#d81159')
drawFigure('fuck', '#8f2d56')
drawFigure('ass', '#218380')
drawFigure('hoes', '#73d2de')
drawFigure('shit', '#4361ee')
drawFigure('pussy', '#560bad')

# Here we modify the tickangle of the xaxis, resulting in rotated labels.
fig.update_layout(barmode='group', xaxis_tickangle=0)
fig.show()
fig.write_image("image.png", width=2360, height=1080)