import lyricsgenius as lg
import re
import json
import key

genius = lg.Genius(key.Genius, skip_non_songs=True, excluded_terms=["(Remix)", "(Live)"], remove_section_headers=True)

artists = [*]

def get_lyrics(art, k):
	dic = {}
	while True:
	    try:
	        songs = (genius.search_artist(art, max_songs=k, sort='popularity')).songs
	        break
	    except:
	        pass

	for song in songs:
		words = re.findall(r"\b([\w?']+)\b", song.lyrics)
		for w in words:
			w = w.lower()
			if w in dic.keys():
				dic[w] = dic[w] + 1
			else:
				dic[w] = 1

	sdic = dic
	return sdic

for a in artists:

	with open('data/' + a +'.txt', 'w') as file:
		file.write(json.dumps(get_lyrics(a, 50)))